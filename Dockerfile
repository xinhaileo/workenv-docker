FROM centos:7

# update yum list
RUN yum -y update

# Install service
RUN yum -y install vim 
RUN yum -y install net-tools 
RUN yum -y install sudo 
RUN yum -y install wget unzip tree telnet
RUN yum -y install openssh* 
RUN yum -y --enablerepo=extras install epel-release
RUN yum -y install python-pip 
RUN yum -y install golang 
RUN yum -y install ansible 
RUN yum -y install install python-lxml 
RUN /usr/bin/pip install python-jenkins 
RUN /usr/bin/pip install PyVmomi pywinrm
RUN /usr/bin/pip install ansible==2.9.9
# change time zone
RUN /bin/cp /usr/share/zoneinfo/Asia/Taipei /etc/localtime && echo 'Asia/Taipei' >/etc/timezone

# install terraform
RUN cd /tmp/ && /usr/bin/wget https://releases.hashicorp.com/terraform/0.12.14/terraform_0.12.14_linux_amd64.zip
RUN cd /tmp/ && /usr/bin/unzip ./terraform_0.12.14_linux_amd64.zip -d /usr/local/bin/
RUN cd /tmp/ && rm ./terraform_0.12.14_linux_amd64.zip 

# install helm
RUN cd /tmp/ && /usr/bin/wget https://get.helm.sh/helm-v3.1.1-linux-amd64.tar.gz
RUN cd /tmp/ && /usr/bin/tar -zxf  ./helm-v3.1.1-linux-amd64.tar.gz && mv ./linux-amd64/helm /usr/local/bin/ && rm ./linux-amd64 -rf
RUN cd /tmp/ && rm ./helm-v3.1.1-linux-amd64.tar.gz

# install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

# install aws-cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

# install aws-iam-authenticator
RUN curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator
RUN chmod +x ./aws-iam-authenticator
RUN mv ./aws-iam-authenticator /usr/local/bin/

# Create user:opuser password:{$passwd}
RUN useradd -ms /bin/bash opuser
RUN echo "opuser:123456121234563456123412345656123411234561234562345656121234563456123412345656123451234566123456" | chpasswd

# create needed folders
RUN mkdir -p /home/opuser/go;

# Start sshd service
RUN /usr/sbin/sshd-keygen -A
EXPOSE 22

# copy ansible prive keys to user .ssh and change owner
RUN echo 'opuser  ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# add go path
RUN echo -e 'export GOPATH=$HOME/go\nexport PATH=$PATH:$GOPATH/bin\nsudo chmod 600 /home/opuser/.ssh/*\nsudo chown -R opuser:opuser /home/opuser' >> /home/opuser/.bash_profile


VOLUME ["/sys/fs/cgroup"]
CMD ["/usr/lib/systemd/systemd"]
CMD ["/usr/sbin/sshd", "-D"]
