## 目的
一個可以直接維護操作的統一開發環境

## 使用方式
會預先把dockerfile build成 image, 存放gitlab    
只需要調整docker-compose.yml就可以使用    
會把go/.ssh/sync目錄掛出來    
可以透過把ansible放置sync目錄底下 在裡面跑相關腳本    
```
version: '2'

services:
    workenv:
        image: registry.gitlab.com/zealot0515/workenv-docker:latest
        # build: ./    (如果想要自己調整 Dockerfile, local build可以用這個)
        container_name: workenv-op
        hostname: workenv-op
        restart: always
        volumes:
            - "./sync/:/home/opuser/sync:rw"
            - "./go/:/home/opuser/go:rw"
            - "./ssh/:/home/opuser/.ssh:rw"
        ports:
            - "50022:22"

```

docker-compose up -d 後透過opuser 登入 密碼->123456121234563456123412345656123411234561234562345656121234563456123412345656123451234566123456    
或者再ssh目錄放入你的pubkey, 用key登入    


